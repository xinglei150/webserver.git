package com.demo.dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList; 

import com.demo.common.BaseConnection;
 
public class BaseDao {
	//获得所有查询结果
    public ArrayList getList(Class cl){
    	ArrayList ar=new ArrayList();
    	Connection conn=BaseConnection.getConnection();
    	PreparedStatement ps=null;
    	ResultSet rs=null;
    	String sql="select * from "+cl.getSimpleName();
    	Field[] fi=cl.getDeclaredFields();
    	try{
    		ps=conn.prepareStatement(sql);
    		rs=ps.executeQuery();
    		while(rs.next()){
    			Object ob=cl.newInstance();
    			for(Field ff:fi){
    				ff.setAccessible(true);
    				ff.set(ob, rs.getObject(ff.getName()));
    			}
    			ar.add(ob);
    		}
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	finally{
    		BaseConnection.closeRes(conn, ps, rs);
    	}
    	return ar;
    }
    
    //根据ID查找数据
    public Object getObById(Class cl,int id){
    	Object ob=null;
    	Connection conn=BaseConnection.getConnection();
    	PreparedStatement ps=null;
    	ResultSet rs=null;
    	Field[] fi=cl.getDeclaredFields();
    	String sql="select * from "+cl.getSimpleName()+" where "+fi[0].getName()+" = "+id;
    	try{
    		ps=conn.prepareStatement(sql);
    		rs=ps.executeQuery();
    		while(rs.next()){
    		    ob=cl.newInstance();
    			for(Field ff:fi){
    				ff.setAccessible(true);
    				ff.set(ob, rs.getObject(ff.getName()));
    			}
    		}
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	finally{
    		BaseConnection.closeRes(conn, ps, rs);
    	}
    	return ob;
    }
    //根据某一条件查询数据
    public ArrayList getListBySome(Class cl,String name,Object value){
    	ArrayList ar=new ArrayList();
    	Connection conn=BaseConnection.getConnection();
    	PreparedStatement ps=null;
    	ResultSet rs=null;
    	String sql="select * from "+cl.getSimpleName()+" where "+name+" = '"+value+"'";
    	Field[] fi=cl.getDeclaredFields();
    	try{
    		ps=conn.prepareStatement(sql);
    		rs=ps.executeQuery();
    		while(rs.next()){
    			Object ob=cl.newInstance();
    			for(Field ff:fi){
    				ff.setAccessible(true);
    				ff.set(ob, rs.getObject(ff.getName()));
    			}
    			ar.add(ob);
    		}
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	finally{
    		BaseConnection.closeRes(conn, ps, rs);
    	}
    	return ar;
    }
    
    //插入数据
    public boolean insert(Object ob){
		boolean b = false;
		Connection conn = BaseConnection.getConnection();
		PreparedStatement ps = null;
		Class cl = ob.getClass();
		Field[] fi = cl.getDeclaredFields();
		//insert into animals (name,age,anid) values(?,?,?)
		String sql = "insert into "+cl.getSimpleName()+" (";
		for(int i = 1;i<fi.length;i++){
			sql = sql+fi[i].getName();
			//4  0 1 2 3
			if(i!=fi.length-1){
				sql = sql+" , ";
			}
		}
		sql = sql+") values (";
		for(int i = 1;i<fi.length;i++){
			sql = sql+" ? ";
			if(i!=fi.length-1){
				sql = sql+" , ";
			}
		}
		sql = sql+")";
		try {
			ps = conn.prepareStatement(sql);
			for(int i = 1;i<fi.length;i++){
				fi[i].setAccessible(true);
				ps.setObject(i, fi[i].get(ob));
			}
			int a = ps.executeUpdate();
			if(a>0){
				b = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			BaseConnection.closeRes(conn, ps);
		}
		return b;
	}
	public boolean insert1(Object ob){
		boolean b = false;
		Connection conn = BaseConnection.getConnection();
		PreparedStatement ps = null;
		Class cl = ob.getClass();
		Field[] fi = cl.getDeclaredFields();
		StringBuffer sb = new StringBuffer();
		//insert into animals (name,age,anid) values(?,?,?)
		sb.append("insert into ");
		sb.append(cl.getSimpleName());
		sb.append(" (");
		for(int i = 1;i<fi.length;i++){
			sb.append(fi[i].getName());
			if(i!=fi.length-1){
				sb.append(" , ");
			}
		}
		sb.append(") values (");
		for(int i = 1;i<fi.length;i++){
			sb.append(" ? ");
			if(i!=fi.length-1){
				sb.append(" , ");
			}
		}
		sb.append(" ) ");
		try {
			ps = conn.prepareStatement(sb.toString());
			for(int i = 1;i<fi.length;i++){
				fi[i].setAccessible(true);
				ps.setObject(i, fi[i].get(ob));
			}
			int a = ps.executeUpdate();
			if(a>0){
				b = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			BaseConnection.closeRes(conn, ps);
		}
		return b;
	}
	//更新
	public boolean update(Object ob){
		boolean b = false;
		Connection conn = BaseConnection.getConnection();
		PreparedStatement ps = null;
		Class cl = ob.getClass();
		Field[] fi = cl.getDeclaredFields();
		StringBuffer sb = new StringBuffer();
		//update animals set name = ?,age = ?,anid = ? where id = ?
		sb.append(" update ");
		sb.append(cl.getSimpleName());
		sb.append(" set ");
		for(int i = 1;i<fi.length;i++){
			fi[i].setAccessible(true);
			sb.append(fi[i].getName());
			sb.append(" = ? ");
			if(i!=fi.length-1){
				sb.append(" , ");
			}
		}
		sb.append(" where ");
		sb.append(fi[0].getName());
		sb.append("=?");
		
		try {
			ps = conn.prepareStatement(sb.toString());
			for(int i = 1;i<fi.length;i++){
				fi[i].setAccessible(true);
				ps.setObject(i, fi[i].get(ob));
			}
			fi[0].setAccessible(true);
			ps.setObject(fi.length, fi[0].get(ob));
			int a = ps.executeUpdate();
			if(a>0){
				b = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			BaseConnection.closeRes(conn, ps);
		}
		return b;
	}
	//按主键删除
	public boolean delete(Class cl , int id){
		boolean b = false;
		Connection conn = BaseConnection.getConnection();
		PreparedStatement ps = null;
		Field[] fi = cl.getDeclaredFields();
		String sql = "delete from "+cl.getSimpleName()+" where "+fi[0].getName()+" = ?";
		try {
			ps = conn.prepareStatement(sql);
			ps.setObject(1, id);
			int a = ps.executeUpdate();
			if(a>0){
				b = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			BaseConnection.closeRes(conn, ps);
		}
	return b ;
	
	}
	//按条件删除
	public boolean deleteBySome(Class cl , String name,Object value){
		boolean b = false;
		Connection conn = BaseConnection.getConnection();
		PreparedStatement ps = null;
		Field[] fi = cl.getDeclaredFields();
		String sql = "delete from "+cl.getSimpleName()+" where "+name+" = ?";
		try {
			ps = conn.prepareStatement(sql);
			ps.setObject(1, value);
			int a = ps.executeUpdate();
			if(a>0){
				b = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			BaseConnection.closeRes(conn, ps);
		}
	return b ;
	
	}
}
