package com.demo.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EncodingFilter implements Filter {
    private String encoding="";
	
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("EncodingFilter初始化过滤器");
        encoding=fConfig.getInitParameter("param");
        System.out.println(encoding);
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		System.out.println("EncodingFilter——doFilter开始");
		String ec=request.getCharacterEncoding();
		if(ec!=null && ec.equals(encoding)){ 
		}
		else{
			request.setCharacterEncoding(encoding);
			response.setCharacterEncoding(encoding);
		}
		chain.doFilter(request, response);
		System.out.println("EncodingFilter——doFilter结束");
	}

	@Override
	public void destroy() {
		System.out.println("销毁过滤器");
	}

}
