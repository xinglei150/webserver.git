package com.demo.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.dao.BaseDao;
import com.demo.entity.Tbl_user;

public class LoginServlet extends HttpServlet { 
	private static final long serialVersionUID = -1870289140466304772L;
 

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException { 
		String username= req.getParameter("username");
		String password =req.getParameter("password");
		System.out.println("username="+username+"  password="+password); 
		BaseDao dao=new BaseDao();
		ArrayList list=dao.getListBySome(Tbl_user.class, "name", username);
		if(list.size()>0){
			//请求转发
			RequestDispatcher rd= req.getRequestDispatcher("success.jsp");
			rd.forward(req, resp);
			//请求重定向
			//resp.sendRedirect("success.jsp");
		}else{
			//请求转发
			RequestDispatcher rd= req.getRequestDispatcher("error.jsp");
			rd.forward(req, resp);
			//resp.sendRedirect("error.jsp");
		}
	}

	@Override
	public void init() throws ServletException {
		System.out.println("servlet初始化");
	}

}
